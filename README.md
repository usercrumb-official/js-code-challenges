# **Code Challenge**

You are free to pick any of the challenges below to show off your awesome programming skills.

You are expected to spend a maxium of ten hours on the challenge of your choosing. We kindly ask you not to spend more than the time suggested.

Please use git for version control and share your solution via a GitHub or Bitbucket repository. The solution should be sent to the following email: challenge@usercrumb.com.


## **Challenge One** - Data Binding Library

Write a simple JavaScript library that binds the values of a JavaScript Object to HTML elements in such a way that when one of the values in the JavaScript Object changes it will be reflected immediately in the DOM.

The following code is how a developer would initialize your library:

```
/**
 * Template (HTML)
 */
const templateString = `
<div class="blog-post-editor">
    <div class="post-preview">
        <h3 class="post-preview__headline">{{headline}}</h3>
        <p class="post-preview__description">{{description}}</p>
    </div>

    <div class="post-creator">
        <input
            class="post-creator__headline"
            type="text"
            placeholder="Enter a headline"
        />
        <textarea 
            class="post-creator__description"
        ></textarea>
    </div>
</div>
`;

/**
 * View engine configuration
 */
const viewConfiguration = {
    template: templateString,
    data: {
        headline: headlineVariable,
        description: descriptionVariable
    },
    appendTo: 'body'
}

/**
 * ViewEngine is your library
 */
const view = new ViewEngine(viewConfiguration);
```

<br>

The solution should be able to handle the following:

1. A change of value in the element with the following class: `.post-creator__headline` must be reflected in the content of the element with the following class: `.post-preview__headline`
2. The following code: `view.data.headline = 'usercrumb'`, should automatically be reflected in both of the following elements: 
    - `.post-creator__headline`
    - `.post-preview__headline`


As bonus you can consider the following:

1. Include binding of the `description` value to the DOM as well, following the same method as the `headline`.
2. Testability of the solution.
3. The performance of the solution.


## **Challenge Two** - Create a Selector Library

Write a JavaScript library that on user click, creates a selector for the element clicked that can be used to identify the exact element later.

The library should be able to build relevant selectors for any of the elements in the following HTML structure:

```
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Challenge Two</title>
    </head>
    <body>
        <nav class="navbar is-fixed-top navbar-fixed-top is-transparent scrolled" role="navigation" aria-label="main navigation">
            <div class="container container-extra is-widescreen">
                <div class="navbar container is-widescreen">
                    <div class="navbar-brand">
                        <a class="navbar-item" href="/">
                            Logo
                        </a>
                    </div>
                        
                    <div class="navbar-end">
                        <a class="navbar-item start-video">
                            Watch introduction
                        </a>
                        <div class="navbar-item">
                            <div class="buttons">
                                <a class="button is-rounded is-light pre-order-scroll">
                                    <strong>Get early access ›</strong>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </nav>
        <section class="section custom-section">
            <div class="container is-widescreen">
                <div class="columns">
                    <div class="column">
                        <h1 class="title is-spaced topbanner-title">
                            Bridging users and software​
                        </h1>
                        <h2 class="topbanner-subtitle subtitle">
                            Bring your users to the finish line with Usercrumb's interactive step-by-step guides.
                        </h2>
                        <div class="buttons section-buttons">
                            <button class="button is-primary is-medium is-rounded pre-order-scroll"><strong>Get early access ›</strong></button>
                        </div>
                    </div>
                    <div class="column topbanner-right-column">
                        <div class="topbanner-container">
                            <img src="images/faqv2.png" class="topbanner-faq">
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section>
            <div>
                <div> 
                    <h1>
                        <i class="fas fa-lightbulb"></i>
                    </h1>
                    <h1>
                        Rethink user adoption
                    </h1>
                    <h2>
                        Interactive user guides have never been easier and more enjoyable.
                    </h2>
                </div>
                <video id="video" poster="/images/player-poster.svg">
                    <source src="https://quiksup-static.ams3.cdn.digitaloceanspaces.com/video/usercrumb-intro.mp4" type="video/mp4">
                    Your browser does not support the video tag.
                </video>
                <img class="simplify illustrations" src="images/simplify-the-use.png">
            </div>
        </section>
    </body>
</html>
```

<br>

The solution should log the selector to the console or in another way display the result after a click.


**Note:** Please describe the considerations you had during the development of the solution. 
